package login.web.TestClass;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestLogin {

	LoginCheck logincheck = new LoginCheck();
	LoginBusiness loginbusiness = new LoginBusiness();
	
	
	@Test
	public void testVerifyLogin() {
		logincheck.setUsername("admin");
		logincheck.setPassword("admin");
		
		String result = loginbusiness.checkLogin(logincheck);
		assertEquals("success", result);
		
	}
	
	
	@Test
	public void testVerifyLogin1() {
		logincheck.setUsername("admin123");
		logincheck.setPassword("admin123");
		
		String result = loginbusiness.checkLogin(logincheck);
		assertEquals("invalid username and password", result);
		
	}
	
	
	@Test
	public void testVerifyLogin2() {
		logincheck.setUsername("123");
		logincheck.setPassword("123");
		
		String result = loginbusiness.checkLogin(logincheck);
		assertEquals("invalid username and password", result);
		
	}
	
	
	@Test
	public void testVerifyLogin3() {
		logincheck.setUsername("admin");
		logincheck.setPassword("123");
		
		String result = loginbusiness.checkLogin(logincheck);
		assertEquals("invalid username and password", result);
		
	}
	
	
	@Test
	public void testVerifyLogin4() {
		logincheck.setUsername("123");
		logincheck.setPassword("admin");
		
		String result = loginbusiness.checkLogin(logincheck);
		assertEquals("invalid username and password", result);
		
	}
	
	
	@Test
	public void testVerifyLogin5() {
		logincheck.setUsername("@#%");
		logincheck.setPassword("#$%&");
		
		String result = loginbusiness.checkLogin(logincheck);
		assertEquals("invalid username and password", result);
		
	}

}
